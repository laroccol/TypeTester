class GameMode {
    constructor()
    {
        this.isTestingRunning = false;
        this.currentChar = 0;

        this.currentWord = 0;
        this.incorrectIndex = 0;
        this.wordsTyped = 0;
        this.startTime = 0;
        this.interval = 0.0;
        this.missedWords = {};
        this.wpmIntervals = [];
        this.wordInterval = 0;
        this.currentwpmInterval = 0;
        this.intervalStart = 0;
        this.words = [];
        this.currentMissedChars = 0;
        this.previousLineOffset = -1;
        this.newLine = false;
        this.lastKey = "";

        this.carretTransforms = [{x: 51, y: 62}];

        this.isCorrect = true;
    }

    onStartTest() {

    }

    onEndTest() {

    }

    reset()
    {

    }

}