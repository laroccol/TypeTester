class GameMode {
    constructor()
    {
        this.isTestingRunning = false;
        this.currentChar = 0;

        this.currentWord = 0;
        this.incorrectIndex = 0;
        this.wordsTyped = 0;
        this.startTime = 0;
        this.interval = 0.0;
        this.missedWords = {};
        this.wpmIntervals = [];
        this.wordInterval = 0;
        this.currentwpmInterval = 0;
        this.intervalStart = 0;
        this.words = [];
        this.currentMissedChars = 0;
        this.previousLineOffset = -1;
        this.newLine = false;
        this.lastKey = "";

        this.carretTransforms = [{x: 51, y: 62}];

        this.isCorrect = true;
    }

    onStartTest() 
    {
        textInput.disabled = false;
        textInput.focus();

        isTestingRunning = true;

        startTime = Date.now();

        wpmIntervals = [0, 0, 0, 0, 0, 0, 0];

        words = textAreaText.split(' ');

        wordInterval = Math.round(words.length / 7);

        intervalStart = Date.now();

        interval = setInterval(updateWPM, 500);
    }

    onEndTest() {

    }

    reset()
    {
        isCorrect = true;

        currentChar = 0;
        currentWord = 0;
        incorrectIndex = 0;

        wordsTyped = 0;

        startTime = 0;
        endTime = 0;

        clearInterval(interval);
        interval = 0.0;
        currentwpmInterval = 0;

        currentMissedChars = 0;

        lastKey = "";

        currentMatchID = 0;

        textInput.value = '';
        
        carretTransforms = [{x: 51, y: 62}];
        newLine = false;
        previousLineOffset = -1;
    }

    updateWPM()
    {
        var wpm = (((currentChar / 5) / (Date.now() - startTime)) * 60000).toFixed(1);
        wpmBox.innerHTML = wpm;
        wpmBar.MaterialProgress.setProgress(clamp((wpm / 200 * 100), 0, 100));
        var red = wpm * 3;
        var green = wpm > 100 ? 255 - ((wpm - 100) ** 1.203) : 255;
        wpmBar.firstChild.style.backgroundColor = "rgb("+red+","+green+",0)";
    }

}