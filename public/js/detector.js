var isTestingRunning = false;
var currentChar = 0;

var currentWord = 0;
var incorrectIndex = 0;
var wordsTyped = 0;
var startTime = 0;
var interval = 0.0;
var missedWords = {};
var wpmIntervals = [];
var wordInterval = 0;
var currentwpmInterval = 0;
var intervalStart = 0;
var words = [];
var currentMissedChars = 0;
var previousLineOffset = -1;
var newLine = false;
var lastKey = "";
var errWord = 0;
var lockOut = false;
var updatedText = "";

var textArea = null;
var textAreaCopy = null;
var textInput = null;
var textAreaText = "";
var wpmBar = null;
var wpmBox = null;
var startButton = null;
var carretTransforms = [{x: 51, y: 62}];

var isCorrect = true;

const passages = ["Specialized uniforms, such as nun and priest garb, can be most helpful. Check out your local uniform store for a wide range of clothes that will get you in, and especially out, of all kinds of stores.",
"It's enough for me to be sure that you and I exist at this moment.",
"We've known each other for many years but this is the first time you've ever come to me for counsel or for help. I can't remember the last time you invited me to your house for a cup of coffee, even though my wife is Godmother to your only child. But, let's be frank here. You never wanted my friendship and you were afraid to be in my debt.",
"The moon belongs to everyone; the best things in life are free. The stars belong to everyone; they gleam there for you and me. The flowers in spring, the robins that sing, the sunbeams that shine - they're yours, they're mine. And love can come to everyone - the best things in life are free!",
"When the shoes first fell from the sky, he remembered thinking that destiny had struck him. Now he thought so again. It was more than a coincidence. It had to be destiny.",
"Higher and higher he climbed. His strength came from somewhere deep inside himself and also seemed to come from the outside as well. After focusing on Big Thumb for so long, it was as if the rock had absorbed his energy and now acted like a kind of giant magnet pulling him toward it.",
"We were playing checkers. I used to kid her once in a while because she wouldn't take her kings out of the back row. But I didn't kid her much though. You never wanted to kid Jane too much. I think I really like it best when you can kid the pants off a girl when the opportunity arises, but it's a funny thing. The girls I like best are ones I never feel much like kidding.",
"In every war, there are calms between storms. There will be days when you lose faith in some of us, days when our allies turn against us, but the day will never come if we forsake this planet and its people.",
"Say what you will. But a person just cannot know what he doesn't know. And you can't always see that a bad thing is going to happen before it happens. If you could, no bad would ever come.",
"There are very few things I wish I could change about those two decades, but there are many things I wish I had known. I wish I had known, for instance, that such a long period of being single was in the cards for me. I also wish I'd known earlier that such a delay was not abnormal among my generation.",
"I'd ask you about love, you'd probably quote me a sonnet. But you've never looked at a woman and been totally vulnerable. Known someone that could level you with her eyes, feeling like God put an angel on earth just for you."];

$(document).ready(() => {
    
    textInput = document.getElementById('typeBar');
    //textInput.disabled = true;

    textArea = document.getElementById('typeText');

    //textAreaCopy = document.getElementById('typeTextCopy');

    wpmBar = document.getElementById('wpmProgress');

    wpmBox = document.getElementById('wpmBox');

    startButton = document.getElementById('startButton');

    $('#typeBar').on('keydown', (event) => {
        onKeyDown(event);
    });

    $('#typeBar').on('keyup', (event) => {
        onKeyUp(event);
    });

    textInput.focus();
});

function onStartTest()
{
    textInput.disabled = false;
    textInput.focus();

    isTestingRunning = true;

    startTime = Date.now();

    wpmIntervals = [0, 0, 0, 0, 0, 0, 0];

    words = textAreaText.split(' ');

    wordInterval = Math.round(words.length / 7);

    intervalStart = Date.now();

    interval = setInterval(updateWPM, 500);
}

function updateWPM()
{
    var wpm = (((currentChar / 5) / (Date.now() - startTime)) * 60000).toFixed(1);
    wpmBox.innerHTML = wpm;
    wpmBar.MaterialProgress.setProgress(clamp((wpm / 200 * 100), 0, 100));
    var red = wpm * 3;
    var green = wpm > 100 ? 255 - ((wpm - 100) ** 1.203) : 255;
    wpmBar.firstChild.style.backgroundColor = "rgb("+red+","+green+",0)";
}


function onEndTest()
{
    isTestingRunning = false;
    var totalChars = textAreaText.length;

    updateWPM();

    var wpm = (((currentChar / 5) / (Date.now() - startTime)) * 60000).toFixed(1);

    if (currentMatchID !== 0)
    {
        socket.emit('updateProgress', currentMatchID, 100, wpm);

        socket.emit('notifyFinish', currentMatchID, wpm);
        document.getElementById('joinQueueButton').disabled = false;
    }


    var numCharactersTyped = 0;
    for (let index = wordsTyped - (words.length % 7); index < wordsTyped; index++) {
        numCharactersTyped += words[index].length;
    }
    wpmIntervals[currentwpmInterval] = parseFloat(((((numCharactersTyped + (words.length % 7) - 1) / 5) / (Date.now() - intervalStart)) * 60000).toFixed(1));

    setIntervalGraph();

    var accuracy = ((textAreaText.length - currentMissedChars) / textAreaText.length * 100).toFixed(1);

    setAccuracyBar(accuracy);

    var data = {wpm: wpm, accuracy: accuracy};

    fetch("/stats", {
        method: "POST",
        headers: {'Content-Type': 'application/json'}, 
        body: JSON.stringify(data)
      }).then(res => {
        console.log("Request complete! response:", res);
      });

    document.getElementById("totalChars").innerHTML = totalChars;
    document.getElementById("wordsMissed").innerHTML = errWord;
    document.getElementById("charsMissed").innerHTML = currentMissedChars;

    document.getElementById("accuracyNumber").innerHTML = accuracy + '<tspan font-size="0.1" dy="-0.07">%</tspan>';

    clearInterval(interval);
    interval = null;

    if (currentMatchID === 0)
    {
        currentPassage = Math.floor(Math.random() * passages.length); 
        textArea.innerHTML = '<span class="blinking-cursor">|</span>' + passages[currentPassage];
        textAreaText = passages[currentPassage];
    }
    else
    {
        textInput.disabled = true;
    }

    document.getElementById('caret').remove();
    
    reset();

    //var tableBody = document.getElementById('missedWordsBody');
    //$("#missedWordsTable tbody tr").remove();

    /*
    var count = 0;
    for (var key in missedWords)
    {
        var row = tableBody.insertRow(count);

        // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);

        // Add some text to the new cells:
        cell1.innerHTML = key;
        cell2.innerHTML = missedWords[key];
        count++;
    }
    */
}

function setAccuracyBar(accuracy)
{
    var xLeft = 0.5
    var yLeft = 1;

    var xRight = 0.5
    var yRight = 0;

    if (accuracy > 75)
    {
        var xRight = (Math.sin((100 - accuracy) * 3.6 * Math.PI / 180) * 0.5) + 0.5;
        var yRight = Math.abs(Math.cos((100 - accuracy) * 3.6 * Math.PI / 180) * 0.5 - 0.5);
    }
    else if (accuracy >= 50)
    {
        var xRight = (Math.sin((180 - ((100 - accuracy) * 3.6)) * Math.PI / 180) * 0.5) + 0.5;
        var yRight = Math.cos((180 - (100 - accuracy) * 3.6) * Math.PI / 180) * 0.5 + 0.5;
    }
    else if (accuracy > 25)
    {
        var xRight = 0.5;
        var yRight = 1;

        var xLeft = 1 - (Math.cos((270 - (100 - accuracy) * 3.6) * Math.PI / 180) * 0.5 + 0.5);
        var yLeft = Math.sin((270 - (100 - accuracy) * 3.6) * Math.PI / 180) * 0.5 + 0.5;
    }
    else
    {
        var xRight = 0.5;
        var yRight = 1;

        var xLeft = 1 - (Math.sin((360 - (100 - accuracy) * 3.6) * Math.PI / 180) * 0.5 + 0.5);
        var yLeft = 1 - (Math.cos((360 - (100 - accuracy) * 3.6) * Math.PI / 180) * 0.5 + 0.5);
    }
    
    document.getElementById("accuracyRight").setAttribute("d", "M 0.5 0.5 0.5 0 A 0.5 0.5 0 0 1 "+xRight+" "+yRight);
    document.getElementById("accuracyLeft").setAttribute("d", "M 0.5 0.5 0.5 1 A 0.5 0.5 0 0 1 "+xLeft+" "+yLeft);
}

function setIntervalGraph()
{
    document.getElementById("wpmGraph").setAttribute("points", "469.3,222.71 1,222.71 1,"+
        (222.71 - wpmIntervals[0])+ 
        " 79.2,"+(222.71 - wpmIntervals[1])+
        " 157.4,"+(222.71 - wpmIntervals[2])+
        " 235.6,"+(222.71 - wpmIntervals[3])+
        " 313.8,"+(222.71 - wpmIntervals[4])+
        " 392.0,"+(222.71 - wpmIntervals[5])+
        " 469.3,"+(222.71 - wpmIntervals[6])
    );

    var maxWPM = 0;
    wpmIntervals.forEach(element => {
        if (element > maxWPM) maxWPM = element;
    });

    document.getElementById("maxWPMGraph").setAttribute("points", "469.3,222.71 1,222.71 1,"+(222.71 - maxWPM)+" 469.3,"+(222.71 - maxWPM));
}

function reset()
{
    currentChar = 0;
    isCorrect = true;
    currentWord = 0;
    errWord = 0;
    incorrectIndex = 0;
    wordsTyped = 0;
    startTime = 0;
    endTime = 0;
    interval = 0.0;
    currentwpmInterval = 0;
    currentMissedChars = 0;
    lastKey = "";
    currentMatchID = 0;
    textInput.value = '';
    carretTransforms = [{x: 51, y: 62}];
    newLine = false;
    previousLineOffset = -1;
}

function onIntervalEnd()
{
    var numCharactersTyped = 0;
    for (let index = wordsTyped - wordInterval; index < wordsTyped; index++) {
        numCharactersTyped += words[index].length;
    }
    wpmIntervals[currentwpmInterval] = parseFloat(((((numCharactersTyped + wordInterval - 1) / 5) / (Date.now() - intervalStart)) * 60000).toFixed(1));
    intervalStart = Date.now();
    currentwpmInterval++;
}

function onKeyUp(event)
{
    var key = event.key;
    if (key === textAreaText[currentChar] && isCorrect)
    {
        if (currentChar >= textAreaText.length - 1)
        {
            onEndTest();
        }
    }
    if (!isTestingRunning)
    {
        console.log("Starting Test");
        onStartTest();
    } 
    if (key === "Backspace")
    {
        if (currentChar > currentWord)
        {
            if (textInput.value == "" && updatedText.length > 0) {
                currentChar-=(counter+1);
                counter = 0;
            } else {
                currentChar--;
                if (counter > 0) {
                    counter--;
                }
            }

            if (isCorrect || currentChar <= incorrectIndex)
            {
                updateTextStyle(currentChar, true)
                //textArea.innerHTML = '<span style="color: #00b9d8;">'+textAreaText.substring(0, currentChar - 1)+'</span><span class="blinking-cursor">|</span>' + textAreaText.substring(currentChar - 1, textAreaText.length);
            }
            else
            {
                updateTextStyle(incorrectIndex, true);
                //textArea.innerHTML = '<span style="color: #00b9d8;">'+textAreaText.substring(0, incorrectIndex)+'</span><span class="blinking-cursor">|</span>' + '<span style="background-color: #ed5353;">'+textAreaText.substring(incorrectIndex, currentChar - 1)+'</span>' + textAreaText.substring(currentChar - 1, textAreaText.length);
            }
            if (currentChar <= incorrectIndex) 
            {
                isCorrect = true;
                textInput.style.backgroundColor = "#263238";
            }
            
        }
        lastKey = key;
        return;
    }
         
}
var counter = 0;
function onKeyDown(event) 
{ 
    var key = event.key;
    updatedText = textInput.value;
    if (!isCorrect && key.length == 1) {
        counter++;
        
    } 
    if (!isTestingRunning)
    {
        console.log("Starting Test");
        onStartTest();
    } 
    console.log(counter);
    /*if (key === "Backspace")
    {
        if (currentChar > currentWord)
        {
            if (textInput.length > updatedText.length) {
                currentChar-=(counter+1);
                counter = 0;
            } else {
                currentChar--;
                if (counter >= 0) {
                    counter--;
                    console.log("Subtracted one from counter, new counter is:", counter);
                }
            }

            if (isCorrect || currentChar <= incorrectIndex)
            {
                updateTextStyle(currentChar, true)
                //textArea.innerHTML = '<span style="color: #00b9d8;">'+textAreaText.substring(0, currentChar - 1)+'</span><span class="blinking-cursor">|</span>' + textAreaText.substring(currentChar - 1, textAreaText.length);
            }
            else
            {
                updateTextStyle(incorrectIndex, true);
                //textArea.innerHTML = '<span style="color: #00b9d8;">'+textAreaText.substring(0, incorrectIndex)+'</span><span class="blinking-cursor">|</span>' + '<span style="background-color: #ed5353;">'+textAreaText.substring(incorrectIndex, currentChar - 1)+'</span>' + textAreaText.substring(currentChar - 1, textAreaText.length);
            }
            if (currentChar <= incorrectIndex) 
            {
                isCorrect = true;
                textInput.style.backgroundColor = "#263238";
            }
            
        }
        lastKey = key;
        return;
    }*/

    if (key.length !== 1) return;
    if (key === ' ' && isCorrect && key === textAreaText[currentChar])
    {
        textInput.value = '';
        currentChar++;
        currentWord = currentChar;
        wordsTyped++;
        lockOut = false;
        updateTextStyle(currentChar, false);

        var currentProgress = wordsTyped / words.length * 100;
        var wpm = (((currentChar / 5) / (Date.now() - startTime)) * 60000).toFixed(1);
        socket.emit('updateProgress', currentMatchID, currentProgress, wpm);

        if (wordsTyped % wordInterval === 0)
        {
            onIntervalEnd();
        }
        lastKey = key;
        return;
    }

    if (key === textAreaText[currentChar] && isCorrect)
    {
        //textArea.innerHTML = '<span style="color: #00b9d8;">'+textAreaText.substring(0, currentChar + 1)+'</span><span class="blinking-cursor">|</span>' + textAreaText.substring(currentChar + 1, textAreaText.length);

        if (currentChar >= textAreaText.length - 1)
        {
            return;
        }
        currentChar++;
        updateTextStyle(currentChar, false);
    }
    else if (key !== textAreaText[currentChar] || !isCorrect)
    {
        if (isCorrect)
        {
            isCorrect = false;
            incorrectIndex = currentChar;
            var wordMissed = words[wordsTyped].toLowerCase();
            missedWords[wordMissed] = (missedWords[wordMissed] + 1) || 1;
            if (lastKey !== "Backspace") currentMissedChars++;
        }
        //textArea.innerHTML = '<span style="color: #00b9d8;">'+textAreaText.substring(0, incorrectIndex)+'</span><span class="blinking-cursor">|</span>' + '<span style="background-color: #ed5353;">'+textAreaText.substring(incorrectIndex, currentChar + 1)+'</span>' + textAreaText.substring(currentChar + 1, textAreaText.length);
        currentChar++;
        updateTextStyle(incorrectIndex, false);
        textInput.style.backgroundColor = 'rgba(255, 0, 0, 0.5)';
    }
    lastKey = key;
    if (!isCorrect && key.length == 1 && lastKey != "Control" && lastKey != "Alt") {
        if (!lockOut) {
            errWord++;
            lockOut = true;
        }
    } 
}

function clamp(num, min, max) 
{
    return num <= min ? min : num >= max ? max : num;
}

function updateTextStyle(incorrectIndex, isBackSpace)
{
    if (currentChar > textAreaText.length) return;

    var caret = document.createElement('span');

    caret.innerHTML = '|';
    caret.className = 'smooth-cursor';
    caret.setAttribute('id', 'caret'); 

    var correct = document.createElement('span');
    correct.style.color = 'white';
    correct.innerText = textAreaText.substring(0, incorrectIndex);

    var incorrect = document.createElement('span');
    incorrect.className = 'errorInput';
    incorrect.innerText = textAreaText.substring(incorrectIndex, currentChar);

    var end = document.createElement('span');
    end.innerText = textAreaText.substring(currentChar, textAreaText.length);

    textArea.innerHTML = '';

    textArea.appendChild(correct);
    textArea.appendChild(incorrect);
    //textArea.appendChild(caret);
    textArea.appendChild(end);

    var changeChar = isBackSpace ? currentChar : currentChar - 1;

    var charWidth = getTextWidth(textAreaText[changeChar], '25pt Helvetica');

    if (isBackSpace) {
        charWidth *= -1;
    }  

    var newX = carretTransforms[changeChar].x + charWidth;
    var newY = carretTransforms[changeChar].y;

    if (!isBackSpace && newLine) {
        newX = 51;
        newY += 38;
    }

    if (isBackSpace)
    {
        newX = carretTransforms[changeChar].x;
        newY = carretTransforms[changeChar].y;

        //lastCaretTransform.x = 
    }

    var currentCaretTransform = isBackSpace ? carretTransforms[currentChar + 1] : carretTransforms[currentChar - 1];
    caret.style.transform = `translate(${newX}px, ${newY}px)`;
    caret.style.setProperty('--startX', `${currentCaretTransform.x}px`);
    caret.style.setProperty('--startY', `${currentCaretTransform.y}px`);
    caret.style.setProperty('--endX', `${newX}px`)
    caret.style.setProperty('--endY', `${newY}px`);
    var container = document.getElementById('typeTextContainer');
    if (container.lastChild.innerText)
    {
        container.replaceChild(caret, container.lastChild);
    }
    else
    {
        container.appendChild(caret);
    }

    if (currentChar >= carretTransforms.length)
    {
        carretTransforms.push({x: newX, y: newY});
    }
    //textArea.innerHTML = '<span style="color: white;">'+textAreaText.substring(0, incorrectIndex)+'</span>' + '<span class="errorInput">'+textAreaText.substring(incorrectIndex, currentChar)+'</span>' + caret.outerHTML + '<span>' + textAreaText.substring(currentChar, textAreaText.length) + '</span>';
    newLine = false;
    if (previousLineOffset !== end.offsetHeight && previousLineOffset !== -1) newLine = true;
    previousLineOffset = end.offsetHeight;

}


function getTextWidth(text, font) {
    // re-use canvas object for better performance
    var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
    var context = canvas.getContext("2d");
    context.font = font;
    var metrics = context.measureText(text);
    return metrics.width;
}