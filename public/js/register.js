let passMatch = false;
function check(input) {
    if (input.value != document.getElementById('textfield_new_password').value) {
        input.setCustomValidity('Password must match');
        passMatch = true;
    } else {
        // input is valid -- reset the error message
        input.setCustomValidity('');
        passMatch = false;
    }
}
function verifyReg(input) {
    let uErr = document.getElementById("regUserError").style.visibility;
    let eErr = document.getElementById("regEmailError").style.visibility;
    let pErr = document.getElementById("regPasswordError").style.visibility;
    let pcErr = passMatch;
    let registerButton = document.getElementById("regButton");
    console.log("User Error: " + uErr, "\nEmail Error: " + eErr, "\nPassword Error: " + pErr, "\nPass Confirm Error: " + pcErr);
    if (uErr != "hidden" || eErr != "hidden" || pErr != "hidden" || pcErr == true) {
        registerButton.disabled = true;
    }
}

$(document).ready(() => {
    $('#textfield_new_username').on('keyup', (event) => {
        let tempUser = document.getElementById("textfield_new_username").value;
        let regUE = document.getElementById("regUserError");
        let textNUP = "^[a-z0-9_-]{3,15}$";
        let registerButton = document.getElementById("regButton");
        registerButton.disabled = false;

        if (tempUser == "") regUE.style.visibility = "hidden";

        $.post('/verifyDuplicate', 
        {
            username: tempUser
        },
        (data, status) => {
            if (data.isValid && tempUser != "") {
                regUE.innerHTML = "This username is already taken, please choose a different one";
                regUE.style.visibility = "visible";
            } else {
                regUE.innerHTML= "3-15 characters with only lowercase letters, digits, underscore, and hyphen";
                if (tempUser.match(textNUP) == null && tempUser != "") {
                    regUE.style.visibility = "visible";
                } else {
                    regUE.style.visibility = "hidden"; 
                }
            }
        });
    });
    $('#textfield_new_email').on('keyup', (event) => {
        let tempEmail = document.getElementById("textfield_new_email").value;
        let regUE = document.getElementById("regEmailError");
        let textNUP = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";
        let registerButton = document.getElementById("regButton");
        registerButton.disabled = false;

        if (tempEmail == "") regUE.style.visibility = "hidden";
        $.post('/verifyDuplicateE', 
        {
            email: tempEmail
        },
        (data, status) => {
            if (data.isValid && tempEmail != "") {
                regUE.innerHTML = "This email is already in use, please verify an account is not already created";
                regUE.style.visibility = "visible";
            } else {
                regUE.innerHTML= "Please enter a valid email address";
                if (tempEmail.match(textNUP) == null && tempEmail != "") {
                    regUE.style.visibility = "visible";  
                } else {
                    regUE.style.visibility = "hidden"; 
                }
            }
        });
        
    });
    $('#textfield_new_password').on('keyup', (event) => {
        let registerButton = document.getElementById("regButton");
        let textPass = document.getElementById("textfield_new_password").value;
        let regUE = document.getElementById("regPasswordError");
        let textNUP = "^([a-zA-Z0-9]|[!@#$%^&()]){6,20}$";
        registerButton.disabled = false;
        if (textPass == "") regUE.style.visibility = "hidden";
        if (textPass.match(textNUP) == null && textPass != "") {
            regUE.style.visibility = "visible";
        } else {
            regUE.style.visibility = "hidden";
        }
    });
    $('#textfield_password_confirm').on('keyup', (event) => {
        let registerButton = document.getElementById("regButton");
        registerButton.disabled = false;
    });
});
/*
pattern="(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|'(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*
                ')@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])
                |1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:
                (?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"
*/