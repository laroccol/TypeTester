var socket = io();
var position = 0;

var myBar = null;

var currentMatchID = 0;

var countdownTimer = null;
var timeLeft = 8;
var timerDelay = 0;

//var testText = 'a quick brown fox jumps over the lazy dog a quick brown fox jumps over the lazy dog a quick brown fox jumps over the lazy dog';

socket.on('startSoloMatch', (passage) => {
    textArea.innerHTML = '<span class="blinking-cursor">|</span>' + passage;
    textAreaText = passage;
});

socket.on('waitingForPlayers', () => {
    console.log("Waiting for players");
    clearInterval(countdownTimer);
    timeLeft = 8;
    document.getElementById('statusMessage').innerHTML = 'Waiting For Players';
    document.getElementById('statusMessage').style.fontSize = '20px';
    document.getElementById('flashingDots').style.display = 'inline-block';
    document.getElementById('raceStatus').style.display = 'inline-block';
});

socket.on('startCountdown', () => {
    console.log("Countdown Started");
    document.getElementById('raceStatus').style.display = 'inline-block';
    document.getElementById('statusMessage').style.fontSize = '40px';
    document.getElementById('flashingDots').style.display = 'none';
    document.getElementById('statusMessage').innerHTML = timeLeft;
    console.log('Time Left: '+timeLeft);
    console.log('Timer Delay: '+timerDelay);
    setTimeout(() => {
        countDown();
        countdownTimer = setInterval(countDown, 1000);
    }, timerDelay);
});

socket.on('updateProgress', (connectionID, progress, wpm) => {
    console.log("HERERERE");
    var barToUpdate = document.getElementById(connectionID);
    if (barToUpdate === null)
    {
        barToUpdate = document.getElementById('myProgress');
    }

    barToUpdate.MaterialProgress.setProgress(progress);
    var red = wpm * 3;
    var green = wpm > 100 ? 255 - ((wpm - 100) ** 1.203) : 255;
    barToUpdate.firstChild.style.backgroundColor = "rgb("+red+","+green+",0)";
});

socket.on('notifyJoin', (connectionID, playerInformation) => {
    console.log('Player Joined Room');
    socket.emit('notifyNewPlayer', connectionID);
    addRacer(connectionID, playerInformation);
});

socket.on('notifyLeave', (connectionID) => {
    removeRacer(connectionID);
});

socket.on('onJoin', (connectionID, playerInformation) => {
    addRacer(connectionID, playerInformation);
});

socket.on('getMatchInformation', (matchID, passage, timeElapsed) => {
    currentMatchID = matchID;
    textArea.innerHTML = passage;
    textAreaText = textArea.innerHTML;
    console.log(timeElapsed / 1000);
    if (timeElapsed === 0)
    {
        timeLeft = 8;
        timerDelay = 0;
    }
    else
    {
        timeLeft = 7 - Math.trunc(timeElapsed / 1000);
        timerDelay = 1000 - (((timeElapsed / 1000) % 1) * 1000);
        console.log(timerDelay);
    }

    /*
    if (startTime !== 0)
    {
        console.log('Elapsed Time: '+((Date.now() - startTime) / 1000));
        timeLeft = 7 - Math.trunc(((Date.now() - startTime) / 1000));
        timerDelay = 1000 - (((Date.now() - startTime) / 1000) % 1) * 1000;
        console.log('Time Left: '+timeLeft);
        console.log('Timer Delay: ' + timerDelay);
    }
    else{
        timeLeft = 8;
        timerDelay = 0;
    }
    */
});

socket.on('notifyFinish', (connectionID, place, WPM) => {
    var textToUpdate = document.getElementById('place'+connectionID);
    if (textToUpdate === null)
    {
        textToUpdate = document.getElementById('place');
    }

    textToUpdate.innerHTML = getPlaceString(place) + ' - ' + WPM + ' WPM';
});

function onJoinQueue()
{
    resetView();
    socket.emit('joinQueue');
    document.getElementById('raceContainer').style.display = 'block';
    document.getElementById('joinQueueButton').disabled = true;
    textInput.value = '';
    textInput.disabled = true;
    reset();
}

function addRacer(connectionID, playerInformation)
{
    var races = document.getElementById('raceBox');
    var newRacerBox = document.createElement('div');
    var newRacerName = document.createElement('div');
    var newRacerProgress = document.createElement('div');
    var newRacerPlace = document.createElement('div');

    newRacerName.innerHTML = playerInformation.name;

    newRacerProgress.setAttribute('id', connectionID);
    newRacerPlace.setAttribute('id', 'place'+connectionID);
    
    newRacerBox.className = 'race';
    newRacerName.className = 'typo-styles__demo mdl-typography--headline info-text-small';
    newRacerProgress.className = 'mdl-progress mdl-js-progress progressBar raceBar';
    newRacerPlace.className = 'typo-styles__demo mdl-typography--headline info-text-small placeText';
    componentHandler.upgradeElement(newRacerProgress);

    newRacerBox.appendChild(newRacerName);
    newRacerBox.appendChild(newRacerProgress);
    newRacerBox.appendChild(newRacerPlace);
    console.log('Adding Racer');
    races.appendChild(newRacerBox);
}

function removeRacer(connectionID)
{
    console.log("REMOVING");
    var racerProgress = document.getElementById(connectionID);
    var racerBox = racerProgress.parentNode;
    racerBox.parentNode.removeChild(racerBox);
}

function countDown() 
{
    if (timeLeft === 0)
    {
        clearInterval(countdownTimer);
        document.getElementById('raceStatus').style.display = 'none';
        socket.emit('startMatch', currentMatchID);

        onStartTest();
    }
    document.getElementById('statusMessage').innerHTML = timeLeft;
    timeLeft--;
}

function getPlaceString(place)
{
    switch (place)
    {
        case 1:
            return '1st';
        case 2:
            return '2nd';
        case 3:
            return '3rd';
        case 4:
            return '4th';
        case 5: 
            return '5th';
    }
}

function resetView()
{
    var races = document.getElementById('raceBox');
    while (races.firstChild) {
        races.removeChild(races.firstChild);
    }

    var newRacerBox = document.createElement('div');
    var newRacerName = document.createElement('div');
    var newRacerProgress = document.createElement('div');
    var newRacerPlace = document.createElement('div');

    newRacerName.innerHTML = 'You';

    newRacerProgress.setAttribute('id', 'myProgress');
    newRacerPlace.setAttribute('id', 'place');
    
    newRacerBox.className = 'race';
    newRacerName.className = 'typo-styles__demo mdl-typography--headline info-text-small';
    newRacerProgress.className = 'mdl-progress mdl-js-progress progressBar raceBar';
    newRacerPlace.className = 'typo-styles__demo mdl-typography--headline info-text-small placeText';
    componentHandler.upgradeElement(newRacerProgress);

    newRacerBox.appendChild(newRacerName);
    newRacerBox.appendChild(newRacerProgress);
    newRacerBox.appendChild(newRacerPlace);
    races.appendChild(newRacerBox);
}