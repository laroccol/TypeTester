function setAccuracyBar(accuracy)
{
    var xLeft = 0.5
    var yLeft = 1;

    var xRight = 0.5
    var yRight = 0;

    if (accuracy > 75)
    {
        var xRight = (Math.sin((100 - accuracy) * 3.6 * Math.PI / 180) * 0.5) + 0.5;
        var yRight = Math.abs(Math.cos((100 - accuracy) * 3.6 * Math.PI / 180) * 0.5 - 0.5);
    }
    else if (accuracy >= 50)
    {
        var xRight = (Math.sin((180 - ((100 - accuracy) * 3.6)) * Math.PI / 180) * 0.5) + 0.5;
        var yRight = Math.cos((180 - (100 - accuracy) * 3.6) * Math.PI / 180) * 0.5 + 0.5;
    }
    else if (accuracy > 25)
    {
        var xRight = 0.5;
        var yRight = 1;

        var xLeft = 1 - (Math.cos((270 - (100 - accuracy) * 3.6) * Math.PI / 180) * 0.5 + 0.5);
        var yLeft = Math.sin((270 - (100 - accuracy) * 3.6) * Math.PI / 180) * 0.5 + 0.5;
    }
    else
    {
        var xRight = 0.5;
        var yRight = 1;

        var xLeft = 1 - (Math.sin((360 - (100 - accuracy) * 3.6) * Math.PI / 180) * 0.5 + 0.5);
        var yLeft = 1 - (Math.cos((360 - (100 - accuracy) * 3.6) * Math.PI / 180) * 0.5 + 0.5);
    }
    
    document.getElementById("accuracyRight").setAttribute("d", "M 0.5 0.5 0.5 0 A 0.5 0.5 0 0 1 "+xRight+" "+yRight);
    document.getElementById("accuracyLeft").setAttribute("d", "M 0.5 0.5 0.5 1 A 0.5 0.5 0 0 1 "+xLeft+" "+yLeft);
}