if (process.env.NODE_ENV !== 'production')
{
  require('dotenv').config();
}

const express = require('express');
const { connect } = require('http2');
const app = express();
var path = require('path');
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const bcrypt = require('bcrypt');
const passport = require('passport');
const expressLayouts = require('express-ejs-layouts');
const flash = require('express-flash');
const session = require('express-session');
const methodOverride = require('method-override');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
let passportSocketIO = require('passport.socketio');
let cookieParser = require('cookie-parser');

const Player = require('./models/player');

const statsRouter = require('./routes/stats');
const settingsRouter = require('./routes/settings');

const initializePassport = require('./passport-config');
const { match, doesNotMatch } = require('assert');
const { start } = require('repl');
const player = require('./models/player');
initializePassport(
  passport, 
  async username => {
    const player = await Player.findOne({username: username});
    return player;
  },
  async id => {
    const player = await Player.findOne({_id: id});
    return player;
  }
);

mongoose.connect(process.env.DATABASE_URL, {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;

db.on('error', (err) => {
  console.error(err);
});

db.once('open', () => {
  console.log('Connected to Mongoose');
});

var shouldAuthenticate = true;

var soloMatches = new Map();
var availableMatches = new Map();
var runningMatches = new Map();
const guests = new Map();

const passages = ["Specialized uniforms, such as nun and priest garb, can be most helpful. Check out your local uniform store for a wide range of clothes that will get you in, and especially out, of all kinds of stores.",
"It's enough for me to be sure that you and I exist at this moment.",
"We've known each other for many years but this is the first time you've ever come to me for counsel or for help. I can't remember the last time you invited me to your house for a cup of coffee, even though my wife is Godmother to your only child. But, let's be frank here. You never wanted my friendship and you were afraid to be in my debt.",
"The moon belongs to everyone; the best things in life are free. The stars belong to everyone; they gleam there for you and me. The flowers in spring, the robins that sing, the sunbeams that shine - they're yours, they're mine. And love can come to everyone - the best things in life are free!",
"When the shoes first fell from the sky, he remembered thinking that destiny had struck him. Now he thought so again. It was more than a coincidence. It had to be destiny.",
"Higher and higher he climbed. His strength came from somewhere deep inside himself and also seemed to come from the outside as well. After focusing on Big Thumb for so long, it was as if the rock had absorbed his energy and now acted like a kind of giant magnet pulling him toward it.",
"We were playing checkers. I used to kid her once in a while because she wouldn't take her kings out of the back row. But I didn't kid her much though. You never wanted to kid Jane too much. I think I really like it best when you can kid the pants off a girl when the opportunity arises, but it's a funny thing. The girls I like best are ones I never feel much like kidding.",
"In every war, there are calms between storms. There will be days when you lose faith in some of us, days when our allies turn against us, but the day will never come if we forsake this planet and its people.",
"Say what you will. But a person just cannot know what he doesn't know. And you can't always see that a bad thing is going to happen before it happens. If you could, no bad would ever come.",
"There are very few things I wish I could change about those two decades, but there are many things I wish I had known. I wish I had known, for instance, that such a long period of being single was in the cards for me. I also wish I'd known earlier that such a delay was not abnormal among my generation.",
"I'd ask you about love, you'd probably quote me a sonnet. But you've never looked at a woman and been totally vulnerable. Known someone that could level you with her eyes, feeling like God put an angel on earth just for you."];

var sessionMiddleware = session({
  name: "COOKIE_NAME_HERE",
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
  store: new (require("connect-mongo")(session))({
      url: "mongodb+srv://user:O6vR0YHBvmXWnQTm@cluster0.x0cgw.mongodb.net/typertester?retryWrites=true&w=majority"
  })
});

app.set('view engine', 'ejs');

app.set('views', __dirname + '/views');

app.set('layout', 'layouts/layout');

app.use(express.static(path.join(__dirname, 'public')));

app.use(express.urlencoded({ extended: false}));

app.use(expressLayouts);

app.use(sessionMiddleware);

app.use(methodOverride('_method'));

app.use(flash());

app.use(passport.initialize());

app.use(passport.session());

app.use(cors());

app.use(bodyParser.json());

app.use(cookieParser());

app.use('/stats', statsRouter);

app.use('/settings', settingsRouter);

app.get('/', (req, res) => {
  var username = 'GUEST';
  var profilePicture = 'images/user.png';
  if (req.user) username = req.user.username.toUpperCase();

  var loggedIn = req.isAuthenticated();
  res.render('index', {loggedIn: loggedIn, username: username, pageName: 'Home', profilePicture: profilePicture});
});

app.get('/register', checkNotAuthenticated, (req, res) => {
  var profilePicture = 'images/user.png';
  res.render('account/register', {pageName: 'Register', profilePicture: profilePicture});
});

app.get('/login', checkNotAuthenticated, (req, res) => {
  var profilePicture = 'images/user.png';
  res.render('account/login', {pageName: 'Login', profilePicture: profilePicture});
});

app.post('/verifyDuplicate', checkNotAuthenticated, async (req, res) => {
  let tempUser = req.body.username;
  let muhammadAli = await Player.findOne({username: tempUser});
  res.json({isValid: muhammadAli !== null});
});

app.post('/verifyDuplicateE', checkNotAuthenticated, async (req, res) => {
  let tempEmail = req.body.email;
  let rockyBalboa = await Player.findOne({email: tempEmail});
  res.json({isValid: rockyBalboa !== null});
});

app.post('/register', checkNotAuthenticated, async (req, res) => {
  try {
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    const newPlayer = new Player({
      _id: mongoose.Types.ObjectId(),
      email: req.body.email,
      username: req.body.username,
      password: hashedPassword,
      races: 0,
      averageWPM: 0,
      averageAccuracy: 0,
      fastestRace: 0
    });

    newPlayer.save((err) => {
      if (err) return console.error(err);
    });

    res.redirect('/login');
  } catch {
    res.redirect('/register');
  }
});

app.post('/login', checkNotAuthenticated, passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/login',
  failureFlash: true
}));

app.delete('/logout', (req, res) => {
  req.logOut()
  res.redirect('/');
});

io.use(function(socket, next){
  // Wrap the express middleware
  sessionMiddleware(socket.request, {}, next);
})

io.on('connection', (socket) => {

  const passage = getRandomPassage();
  soloMatches.set(socket.id, {passage: passage, currentWord: '', lastWordTime: 0});
  socket.emit('startSoloMatch', passage);

  socket.on('joinQueue', async () => {
    if (socket.rooms.size > 1)
    {
      const matchID = Array.from(socket.rooms)[1];
      socket.leave(matchID);
    }
    if (availableMatches.size === 0)
    {
      const matchID = Math.floor(Math.random()  * 100000);
      const passage = passages[Math.floor(Math.random() * passages.length)];
      availableMatches.set(matchID, {passage: passage, players: [socket.id] });
      socket.join(matchID);
      io.in(matchID).emit('waitingForPlayers');
      socket.emit('getMatchInformation', matchID, passage, 0);
    }
    else
    {
      const firstMatchID = availableMatches.keys().next().value;
      availableMatches.get(firstMatchID).players.push(socket.id);

      socket.join(firstMatchID);
      const passportUser = socket.request.session.passport;
      var username = 'Guest-'+socket.id.substring(0, 4);
      if (passportUser)
      {
        const userID = passportUser.user;
        var player = await Player.findById(userID);
        if (player)
        {
          username = player.username;
        }
      }
      socket.to(firstMatchID).emit('notifyJoin', socket.id, { name: username});
      const startTime = availableMatches.get(firstMatchID).startTime;
      var timeElapsed = 0;
      if (startTime)
      {
        timeElapsed = Date.now() - startTime;
      }
      socket.emit('getMatchInformation', firstMatchID, availableMatches.get(firstMatchID).passage, timeElapsed);

      if (availableMatches.get(firstMatchID).players.length == 2)
      {
        availableMatches.get(firstMatchID).startTime = Date.now();
        io.in(firstMatchID).emit('startCountdown');
      }
      if (availableMatches.get(firstMatchID).players.length >= 3)
      {
        socket.emit('startCountdown');
        //io.in(availableMatches[0].matchID).emit('raceStarted', availableMatches[0].matchID);
        runningMatches.set(firstMatchID, availableMatches.get(firstMatchID));
        availableMatches.delete(firstMatchID);
      }
    }
  });

  socket.on('startMatch', (matchID) => {
    const availableMatch = availableMatches.get(matchID);
    if (availableMatch)
    {
      runningMatches.set(matchID, availableMatch);
      availableMatches.delete(matchID);
    }
  });

  socket.on('notifyNewPlayer', async (connectionID) => {
    const passportUser = socket.request.session.passport;
    var username = 'Guest-'+socket.id.substring(0, 4);
    if (passportUser)
    {
      const userID = passportUser.user;
      var player = await Player.findById(userID);
      if (player)
      {
        username = player.username;
      }
    }
    socket.to(connectionID).emit('onJoin', socket.id, { name: username});
  });

  socket.on('notifyFinish', (matchID, WPM) => {
    const runningMatch = runningMatches.get(matchID);
    if (runningMatch)
    {
      runningMatches.get(matchID).finishedPlayers = (runningMatch.finishedPlayers + 1) || 1;
      var place = runningMatch.finishedPlayers;
      io.in(matchID).emit('notifyFinish', socket.id, place, WPM);
      if (runningMatch.finishedPlayers === runningMatch.players.length)
      {
        var room = io.sockets.adapter.rooms.get(matchID);
        room.forEach(socketID => {
          io.sockets.sockets.get(socketID).leave(matchID);
        });
        runningMatches.delete(matchID);    
      }
    }
    //socket.leave(matchID);
  });

  socket.on('disconnecting', () => {
    console.log(socket.rooms);
    if (socket.rooms.length <= 1) return;

    const matchID = Array.from(socket.rooms)[1];

    var availableMatch = availableMatches.get(matchID);

    if (availableMatch)
    {
      const playerIndex = availableMatch.players.indexOf(socket.id);
      availableMatch.players.splice(playerIndex, 1);

      if (availableMatch.players.length === 0)
      {
        availableMatches.delete(matchID);
      }
      else if (availableMatch.players.length === 1)
      {
        io.in(matchID).emit('waitingForPlayers');
        delete availableMatches.get(matchID).startTime;
      }
      socket.to(matchID).emit('notifyLeave', socket.id);
      return;
    }
    
    var runningMatch = runningMatches.get(matchID);

    if (runningMatch)
    {
      const playerIndex = runningMatch.players.indexOf(socket.id);
      runningMatch.players.splice(playerIndex, 1);
      if (runningMatch.players.length === 0) runningMatches.delete(matchID);
      return;
    }
  });

  socket.on('chat message', (msg) => {
    console.log('message: ' + msg);
  });

  socket.on('updateProgress', (matchID, progress, wpm) => {
    io.in(matchID).emit('updateProgress', socket.id, progress, wpm);
  });
});

http.listen(process.env.PORT || 80, () => {
  console.log('listening on *:80');
});

function checkAuthenticated(req, res, next)
{
  if (!shouldAuthenticate) return next();

  if (req.isAuthenticated())
  {
    return next();
  }

  return res.redirect('/login');
}

function checkNotAuthenticated(req, res, next) 
{
  if (!shouldAuthenticate) return next();

  if (req.isAuthenticated())
  {
    return res.redirect('/');
  }

  return next();
}

function getRandomPassage()
{
  return passages[Math.floor(Math.random() * passages.length)];
}