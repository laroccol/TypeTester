const mongoose = require('mongoose');

const playerSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email: String,
    username: String,
    password: String,
    races: Number,
    averageWPM: Number,
    averageAccuracy: Number,
    fastestRace: Number,
    profilePicture: String
});

module.exports = mongoose.model('Player', playerSchema, "players");