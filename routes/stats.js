const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const Player = require('../models/player');

router.get('/', (req, res) => {
    var username = 'GUEST';
    var averageWPM = 0;
    var accuracy = 0;
    var fastestRace = 0;
    var profilePicture = 'images/user.png';
    if (req.user) 
    {
        username = req.user.username.toUpperCase();
        averageWPM = req.user.averageWPM;
        accuracy = req.user.averageAccuracy;
        fastestRace = req.user.fastestRace;
    }

    var loggedIn = req.isAuthenticated();

    res.render('stats/playerStats', {pageName: 'Stats', loggedIn: loggedIn, username: username, averageWPM: averageWPM, accuracy: accuracy, fastestRace: fastestRace, profilePicture: profilePicture});
});

router.post('/', bodyParser.json(), async (req, res) => {
    if (req.isAuthenticated())
    {
        const player = await Player.findById(req.user._id);
        if (player)
        {
            const oldRaces = player.races;
            const oldAverageWPM = player.averageWPM;
            const oldAverageAccuracy = player.averageAccuracy;
            const oldFastestRace = player.fastestRace;

            const wpm = parseFloat(req.body.wpm)
            const accuracy = parseFloat(req.body.accuracy);

            var newRaces = oldRaces + 1;
            var newAverageWPM = parseFloat((((oldAverageWPM*oldRaces) + wpm)/(newRaces)).toFixed(1));
            var newAverageAccuracy = parseFloat((((oldAverageAccuracy*oldRaces) + accuracy)/(newRaces)).toFixed(1));
            var newFastestRace = wpm > oldFastestRace ? wpm : oldFastestRace;

            var newPlayer = await Player.findByIdAndUpdate(req.user._id, { averageWPM: newAverageWPM, averageAccuracy: newAverageAccuracy, races: newRaces, fastestRace: newFastestRace });
        }
    }
    res.send("Good");
});

module.exports = router;