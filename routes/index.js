const express = require('express');
const router = express.Router();

router.get('/', checkAuthenticated, (req, res) => {
    res.render('index');
  });

function checkAuthenticated(req, res, next)
{
  if (req.isAuthenticated())
  {
    return next();
  }

  res.redirect('/account/login');
}

module.exports = router;